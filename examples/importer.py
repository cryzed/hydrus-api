#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import collections
import enum
import collections.abc as abc
import pathlib
import itertools

import hydrus_api
import hydrus_api.utils

REQUIRED_PERMISSIONS = (hydrus_api.Permission.IMPORT_FILES, hydrus_api.Permission.ADD_TAGS)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("api_key")
    parser.add_argument("paths", type=pathlib.Path, nargs="+")
    parser.add_argument("--tag", "-t", action="append", dest="tags", default=[])
    parser.add_argument("--tag-services", "-T", dest="tag_services", action="append")
    parser.add_argument("--no-recursive", "-r", action="store_false", dest="recursive")
    parser.add_argument("--no-read-metadata", "-m", action="store_false", dest="read_metadata")
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    return parser


def import_path(
    client: hydrus_api.Client,
    path: pathlib.Path,
    tags: abc.Iterable[str],
    tag_service_keys: abc.Iterable[str],
    read_metadata: bool,
    recursive: bool,
) -> None:
    tag_sets = collections.defaultdict(set)

    # Make sure to also iterate over the initial path itself
    for path in itertools.chain((path,), path.rglob("*") if recursive else path.glob("*")):
        if path.suffix.lower() == ".txt" or not path.is_file():
            continue

        all_tags = set(tags)
        if read_metadata and (metadata_path := path.with_name(f"{path.name}.txt")).is_file():
            all_tags.update(
                tag
                for line in metadata_path.read_text(hydrus_api.HYDRUS_METADATA_ENCODING).splitlines()
                if (tag := line.strip())
            )
        tag_sets[tuple(sorted(all_tags))].add(path.resolve())

    for tag_set, paths in tag_sets.items():
        # Open and read every file, in case we want to import into a remote Hydrus instance. Unfortunately we can't open
        # all files at once, that might easily cause us to run into some system-limits
        for path in paths:
            with open(path, "rb") as file:
                hydrus_api.utils.add_and_tag_files(client, (file,), tag_set, tag_service_keys)


def main(arguments: argparse.Namespace) -> ExitCode:
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ExitCode.FAILURE

    # Translate passed tag-service keys or names into keys. If there are multiple services with the same name we just
    # take the first one
    service_mapping = hydrus_api.utils.get_service_mapping(client)
    tag_service_keys = [
        service_mapping[name_or_key][0] if name_or_key in service_mapping else name_or_key
        for name_or_key in arguments.tag_services or ("my tags",)
    ]
    for path in arguments.paths:
        print(f"Importing {path}...")
        import_path(client, path, arguments.tags, tag_service_keys, arguments.read_metadata, arguments.recursive)

    return ExitCode.SUCCESS


if __name__ == "__main__":
    parser = get_argument_parser()
    arguments = parser.parse_args()
    parser.exit(main(arguments))
